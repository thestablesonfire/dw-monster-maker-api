var express = require("express");
var router = express.Router();
var ObjectId = require("mongodb").ObjectID;
var MonsterListService = require("../services/monsterList.service");
var monsterListService = new MonsterListService();

router.get("/:userId", function (req, res, next) {
  monsterListService.getMonsterListsForUser(req, res);
});

router.post("/:userId", function (req, res, next) {
  monsterListService.addOrUpdateList(req, res);
});

module.exports = router;
