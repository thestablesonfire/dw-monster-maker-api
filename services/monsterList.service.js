var MonsterService = require("./monster.service");
var monsterService = new MonsterService();
var RequestService = require("./request.service");
var requestService = new RequestService();
var ObjectID = require('mongodb').ObjectID;

var MonsterListService = function () {
  this.addOrUpdateList = addOrUpdateList;
  this.getMonsterListsForUser = getMonsterListsForUser;

  function addOrUpdateList(req, res) {
    var id = req.params.userId;
    var collection = getMonsterListCollection(req);
    var monsterIds = getMonsterIds(req.body.list.monsters);

    if (requestService.checkUser(req, id)) {
      var listId = req.body.list.id;
      var setId = false;

      if (!listId) {
        listId = ObjectID();    
        setId = true;
      }

      // Update monster values
      collection.update(
        { userId: id },
        {
          $set: {
            _id: listId,
            userId: id,
            name: req.body.list.name,
            monsters: monsterIds,
          },
        },
        { upsert: true },
        function (err, result) {
          monsterService.addOrUpdateMonsters(req, res, listId, req.body.list.monsters)
          requestService.printMsg(res, err, "updated");
        }
      );
    } else {
      requestService.returnUnauthorized(res);
    }
  }

  function getMonsterListsForUser(req, res) {
    var id = req.params.userId;
    var collection = getMonsterListCollection(req);
    var endResponse;

    if (requestService.checkUser(req, id)) {
      collection.find({ userId: id }, {}).then((lists) => {
        endResponse = lists;

        lists.forEach((list, index) => {
          monsterService.getMonsters(req, list.monsters).then((monsters) => {
            list.monsters = monsters;
          }).then(() => {
            if (index === lists.length - 1) {
              res.send(endResponse);
            }
          });
        });
      });
    } else {
      requestService.returnUnauthorized(res);
    }
  }

  function getMonsterListCollection(req) {
    return requestService.getCollection(req, "lists");
  }

  function getMonsterIds(monsters) {
    return monsters.map((monster) => {
      return monster.id;
    });
  }
};

module.exports = MonsterListService;
