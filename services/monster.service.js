var RequestService = require("./request.service");
var requestService = new RequestService();

var MonsterService = function () {
  this.addOrUpdateMonsters = addOrUpdateMonsters;
  this.getMonsters = getMonsters;

  function addOrUpdateMonsters(req, res, listId, monsters) {
    var monsterCollection = getMonsterCollection(req);
    var errorUpdatingMonsters = false;

    monsters.forEach((monster) => {
      monsterCollection.update(
        { id: monster.id  },
        {
          $set: {
            listId: listId,
            moves: monster.moves,
            monsterTags: monster.monsterTags,
            weaponTags: monster.weaponTags,
            id: monster.id,
            name: monster.name,
            description: monster.description
          },
        },
        { "upsert": true  },
        function (err, result) {
          if (err) {
            errorUpdatingMonsters = true;
          } 
        }
      );
    });

    return !errorUpdatingMonsters;
  }

  function getMonsters(req, monsterIds) {
    var collection = getMonsterCollection(req);

    return collection.find({ "id": { $in: monsterIds } });
  }

  function getMonsterCollection(req) {
    return requestService.getCollection(req, "monsters");
  }
};

module.exports = MonsterService;
