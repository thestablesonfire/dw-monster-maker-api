var express = require("express");
var path = require("path");
// var favicon = require('serve-favicon');
// var logger = require('morgan');
// var cookieParser = require('cookie-parser');
var bodyParser = require("body-parser");
var jwt = require("jsonwebtoken");
var config = require("./config");
var monk = require("monk");
var db = monk("0.0.0.0:27017/monster-maker-api");
var index = require("./routes/index");
var token = require("./routes/token");
var list = require("./routes/monsterList");
var app = express();

app.set("superSecret", config.secret);

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// Make our db accessible to our router
app.use(connectDb);
setAllowedHeaders();
app.use(validateRequest);
setRoutes();
app.use(set404);
app.use(set500);

function connectDb(req, res, next) {
  req.db = db;
  next();
}

function rejectRequest(res) {
  return res.status(403).send({
    success: false,
    message: "No token provided.",
  });
}

function set404(req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
}

function set500(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
}

function setAllowedHeaders() {
  app.all("/*", function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Methods",
      "GET, PUT, POST, DELETE, OPTIONS"
    );
    res.header(
      "Access-Control-Allow-Headers",
      "Content-Type, Authorization, Content-Length, X-Requested-With, X-Access-Token"
    );
    next();
  });
}

function setRoutes() {
  app.use("/", index);
  app.use("/token", token);
  app.use("/monsterList", list);
}

function validateRequest(req, res, next) {
  var token =
    req.body.token || req.query.token || req.headers["x-access-token"];

  if (req.method === "OPTIONS") {
    res.send("op");
  }

  if (token) {
    validateToken(token, req, res, next);
  } else {
    rejectRequest(res);
  }
}

function validateToken(token, req, res, next) {
  jwt.verify(token, app.get("superSecret"), function (err, decoded) {
    if (err) {
      return res.json({
        success: false,
        message: "Failed to authenticate token.",
      });
    } else {
      req.decoded = decoded;
      next();
    }
  });
}

module.exports = app;
